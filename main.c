#include <avr/io.h>
#include <util/delay.h>


int delay=10;
unsigned char color=0;
int main(){

DDRB &= ~(1 << 7);// initialise the button pin as output
int Pressed = 0; //Initialize/Declare the Pressed variable


while(1){
if(color==0){
	DDRB =1<<0;
	PORTB=1<<0;
	_delay_ms (delay);
	PORTB&=~(1<<0);
	_delay_ms(delay);
	DDRB &=~(1<<0);
	}

if(color==1){
	DDRB =1<<1;
	PORTB=1<<1;
	_delay_ms (delay);
	PORTB&=~(1<<1);
	_delay_ms(delay);
	DDRB &=~(1<<1);
	}
if(color==2){
	DDRB =1<<2;
	PORTB=1<<2;
	_delay_ms (delay);
	PORTB&=~(1<<2);
	_delay_ms(delay);
	DDRB &=~(1<<2);
	}
// read the button 

if (bit_is_clear(PINA, 7)) //Check is the button is pressed
	{
//Make sure that the button was released first
	if (Pressed == 0) 
	{
		// do something if button is pressed
		Pressed=1;
		color++;
		if(color>2)color=0;
	}
}
else
	{
	//This code executes when the button is not pressed.
	Pressed = 0;
	}

}
return 0;

}