/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int red= 8;
int green=9;
int blue=10;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(red, OUTPUT);
  pinMode(green,OUTPUT); 
  pinMode(blue, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(red, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  digitalWrite(red, HIGH);    // turn the LED off by making the voltage LOW
  delay(100);               // wait for a second
  
  digitalWrite(green, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  digitalWrite(green, HIGH);    // turn the LED off by making the voltage LOW
  delay(100);  

digitalWrite(blue, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  digitalWrite(blue, HIGH);    // turn the LED off by making the voltage LOW
  delay(100);    
}
